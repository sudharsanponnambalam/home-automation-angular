import { Component, OnInit } from '@angular/core';
import {DeviceTransaction} from '../device-transaction'
import { DeviceTransactionHttpClientService } from '../device-transaction-http-client.service';

@Component({
  selector: 'app-device-transaction-list',
  templateUrl: './device-transaction-list.component.html',
  styleUrls: ['./device-transaction-list.component.css']
})
export class DeviceTransactionListComponent implements OnInit {

  deviceTransactions: DeviceTransaction[] | undefined;

  constructor(private deviceTransactionHttpClientService : DeviceTransactionHttpClientService) { }

  ngOnInit(): void {
    this.getDeviceList();
  }

  getDeviceList(){
    return this.deviceTransactionHttpClientService.getDevices().subscribe(responseData=>{
      this.deviceTransactions=responseData;
    });
  }
}
