import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceTransactionListComponent } from './device-transaction-list.component';

describe('DeviceTransactionListComponent', () => {
  let component: DeviceTransactionListComponent;
  let fixture: ComponentFixture<DeviceTransactionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeviceTransactionListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DeviceTransactionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
