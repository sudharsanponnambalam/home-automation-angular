import { TestBed } from '@angular/core/testing';

import { DeviceTransactionHttpClientService } from './device-transaction-http-client.service';

describe('DeviceTransactionHttpClientService', () => {
  let service: DeviceTransactionHttpClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeviceTransactionHttpClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
