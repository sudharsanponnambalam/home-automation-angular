import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceTransaction } from '../device-transaction';
import { DeviceTransactionHttpClientService } from '../device-transaction-http-client.service';

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.css']
})
export class AddDeviceComponent implements OnInit {

  deviceDetail: DeviceTransaction = new DeviceTransaction();
  constructor(private deviceHttpClientService: DeviceTransactionHttpClientService,
                private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    console.log(this.deviceDetail);
    this.deviceHttpClientService.addNewDevice(this.deviceDetail).subscribe(responseData=>{
     console.log(responseData);
     this.router.navigate(["/devices"]);
    },error=>console.error(error));
    
  }

}
