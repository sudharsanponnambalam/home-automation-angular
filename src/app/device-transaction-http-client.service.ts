import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DeviceTransaction } from './device-transaction';

@Injectable({
  providedIn: 'root'
})
export class DeviceTransactionHttpClientService {

  constructor(private httpClientModule: HttpClient) { }

  private baseUrl = "http://localhost:8080/device";

  getDevices(): Observable<DeviceTransaction[]> {
    return this.httpClientModule.get<DeviceTransaction[]>(this.baseUrl + "/retrieve-all");
  }

  addNewDevice(deviceTransaction: DeviceTransaction): Observable<Object> {
    return this.httpClientModule.post(this.baseUrl + "/add", deviceTransaction,{responseType: 'text' });
  }
}
