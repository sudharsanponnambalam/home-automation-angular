import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeviceTransactionListComponent } from './device-transaction-list/device-transaction-list.component';
import { AddDeviceComponent } from './add-device/add-device.component';

const routes: Routes = [
  {path:'devices', component: DeviceTransactionListComponent}, 
  {path:'', redirectTo:'devices', pathMatch:'full'},
  {path:'add-device', component: AddDeviceComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
