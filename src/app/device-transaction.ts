export class DeviceTransaction {
    deviceId: string;
    deviceName: string
    deviceStatus: string;
    userId: string;
    createdAt: Date;
    modifiedAt: Date;
}
